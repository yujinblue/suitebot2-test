package suitebot2.server;

public interface SimpleRequestHandler
{
	String processRequest(String request);
}
